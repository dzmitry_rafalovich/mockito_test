package by.shag.service;

import java.util.List;
import java.util.stream.Collectors;

import by.shag.api.dto.UserDto;
import by.shag.jpa.model.User;
import by.shag.jpa.repository.UserRepository;
import by.shag.mapping.UserDtoMapper;


public class UserService {
    
    private UserRepository userRepository;
    
    private UserDtoMapper mapper;
    

    public UserDto save(UserDto dto) {
        User user = mapper.map(dto);
        User saved = userRepository.save(user);
        userRepository.save(user);
        return mapper.map(saved);
    }

    public UserDto findById(Integer id) {
        User user = userRepository.findById(id);
        return mapper.map(user);
    }

    public List<UserDto> findAll() {
        return userRepository.findAll()
                .stream()
                .map(user -> mapper.map(user))
                .collect(Collectors.toList());
    }

    public UserDto update(UserDto dto) {
        User user = mapper.map(dto);
        User updated = userRepository.update(user);
        return mapper.map(updated);
    }

    public void delete(Integer id) {
        userRepository.delete(id);
    }
}
